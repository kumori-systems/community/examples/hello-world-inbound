#!/bin/bash

# Cluster variables
CLUSTERNAME="..."
REFERENCEDOMAIN="test.kumori.cloud"
CLUSTERCERT="cluster.core/wildcard-test-kumori-cloud"

# Service variables
DEPLOYNAME="helloworldinbdep"
DOMAIN="helloworldinbdomain"
SERVICEURL="helloworldinb-${CLUSTERNAME}.${REFERENCEDOMAIN}"

KUMORI_SERVICE_MODEL_VERSION="1.1.6"
KUMORI_INBOUND_VERSION="1.3.0"

# Change if special binaries want to be used
# Tested with kam v0.7.0-alpha24
KAM_CMD="kam"
KAM_CTL_CMD="kam ctl"

case $1 in

'refresh-dependencies')
  cd manifests
  # Dependencies are removed and added again just to ensure that the right versions
  # are used.
  # This is not necessary, if the versions do not change!
  ${KAM_CMD} mod dependency --delete kumori.systems/kumori
  ${KAM_CMD} mod dependency --delete kumori.systems/builtins/inbound
  ${KAM_CMD} mod dependency kumori.systems/kumori/@${KUMORI_SERVICE_MODEL_VERSION}
  ${KAM_CMD} mod dependency kumori.systems/builtins/inbound/@${KUMORI_INBOUND_VERSION}
  # Just for sanitize: clean the directory containing dependencies
  rm -rf ./cue.mod
  cd ..
  ;;

'create-domain')
  ${KAM_CTL_CMD} register domain $DOMAIN --domain $SERVICEURL
  ;;

# To check if our module is correct (from the point of view of the CUE syntax and
# the Kumori service model), we can use "kam process" to enerate the JSON
# representation
'dry-run')
  ${KAM_CMD} process deployment -t ./manifests
  ;;


'deploy-service')
  ${KAM_CMD} service deploy -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "Hello world service" \
    --wait 5m
  ;;

# Currently, "kam service update" is not reporting changes to used ("kumorictl update"
# command do it) -- https://gitlab.com/kumori/project-management/-/issues/1440#note_1547945612
'update-service')
  ${KAM_CMD} service update -d deployment -t ./manifests $DEPLOYNAME -- \
    --comment "Updating hello world service" \
    --wait 5m
  ;;

'describe')
  ${KAM_CMD} service describe $DEPLOYNAME
  ;;

'test')
  curl https://${SERVICEURL}/hello
  ;;

'undeploy-service')
  ${KAM_CMD} service undeploy $DEPLOYNAME -- --wait 5m
  ;;

'delete-domain')
  ${KAM_CTL_CMD} unregister domain $DOMAIN
  ;;

*)
  echo "This script doesn't contain that command"
  ;;

esac
