package deployment

import (
  s ".../service:service"
)

#Deployment: { // MANDATORY name
  name: "helloworlddep"
  artifact: s.#Artifact
  config: {
    parameter: {}
    resource: {
      servercert: { certificate: "cluster.core/wildcard-test-kumori-cloud" }
      serverdomain:  { domain: "helloworldinbdomain" }
    }
    scale: detail: {
      frontend: hsize: 2
      // Httpinbound is an special role (inbound builtin service), so it is
      // not required to define its size
      // httpinbound: hsize: 1
    }
    resilience: 1
  }
}
